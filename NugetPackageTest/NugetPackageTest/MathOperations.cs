﻿namespace NugetPackageTest
{
    public class MathOperations
    {
        public int fibonacci(int duration)
        {
            int previousNum = 0;
            int num = 1;
            int temp;

            for(int count = 0; count < duration; count++)
            {
                temp = num;
                num += previousNum;
                previousNum = temp;
            }

            return previousNum;
        }
        public int recfibonacci(int previousNum, int num, int duration, int count)
        {
            if(count != duration)
                previousNum = recfibonacci(num, previousNum + num, duration, count + 1);

            return previousNum;
        }
    }
}
